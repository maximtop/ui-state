import React from 'react' // eslint-disable-line

const filters = [
  [
    'all',
    'all'],
  [
    'active',
    'active'],
  [
    'finished',
    'finished']]

export default class TasksList extends React.Component {
  // BEGIN (write your solution here)
  state = {
    tasks: this.props.tasks,
    tasksUIState: [],
    viewState: 'all',
  }
  
  toggleState = (filter) => {
    switch (filter) {
      case 'all': {
        this.setState({ tasksUIState: this.state.tasks })
        break;
      }
      case 'active': {
        const activeTasks = this.state.tasks.filter(
          task => task.status === 'active')
        this.setState({ tasksUIState: activeTasks })
        break;
      }
      case 'finished': {
        const finishedTasks = this.state.tasks.filter(
          task => task.status === 'finished')
        this.setState({ tasksUIState: finishedTasks })
        break;
      }
    }
  }
  
  renderFilters = (viewState) => {
    return (
      <div className="col-8 mt-3 d-flex justify-content-around">
        {viewState === 'all' ? 'all' : <a className="app-filter-all" href="#"
                                          onClick={this.toggleState(
                                            'all')}>all</a>}
        {viewState === 'active' ? 'active' :
          <a className="app-filter-active" href="#"
             onClick={this.toggleState('active')}>active</a>}
        {viewState === 'finished' ? 'finished' :
          <a className="app-filter-finished" href="#"
             onClick={this.toggleState('finished')}>finished</a>}
      </div>
    )
    
  }
// END
  
  removeTask = id => (e) => {
    e.preventDefault()
    this.props.removeTask({ id })
  }
  
  toggleTaskState = id => (e) => {
    e.preventDefault()
    this.props.toggleTaskState({ id })
  }

// BEGIN (write your solution here)
  renderTasks () {
    const tasks = this.props.tasks
    return (
      tasks.map(({ id, state, text }) => {
        return (
          <li key={id} className="list-group-item d-flex justify-content-end">
            <a className="app-toggle-state mr-3" href="#"
               onClick={this.toggleTaskState(id)}>-</a>
            <div className="mr-auto">{(state === 'finished'
              ? <s>{text}</s>
              : text)}</div>
            <a className="app-remove-task" href="#"
               onClick={this.removeTask(id)}>x</a>
          </li>
        )
      })
    )
  }

// END
  
  render () {
    const { tasks } = this.props
    
    if (tasks.length === 0) {
      return null
    }
    
    // BEGIN (write your solution here)
    return (
      <div className="mt-3">
        <ul className="list-group">
          {this.renderTasks(this.state.tasksUIState)}
        </ul>
        {this.renderFilters(this.state.viewState)}
      </div>
    )
    // END
  }
}
